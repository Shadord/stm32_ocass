/*
 * function.h
 *
 *  Created on: 25 janv. 2021
 *      Author: Nathan
 */

#ifndef INC_FUNCTION_H_
#define INC_FUNCTION_H_

#include "main.h"
#include "stm32l4xx_hal.h"
#include "time.h"
#include "lmic.h"
#include "debug.h"
#include "adc_utils.h"
#include <stdbool.h>

extern FrequenceCount frequenceCounter;
extern TIM_HandleTypeDef htim6;

float convertFromADC_12B(u2_t value, float voltage);
float GET_Temp(ADC_HandleTypeDef hadc1, uint32_t channel);
void PowerTempSensor(bool on);
float cycle(float* temperature, float* humidite);




#endif /* INC_FUNCTION_H_ */

/*
 * adc_utils.h
 *
 *  Created on: Jan 6, 2021
 *      Author: julien.casanova
 */

uint32_t get_ADC_value(ADC_HandleTypeDef hadc1, uint32_t channel);

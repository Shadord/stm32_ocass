/*
 * adc_utils.c
 *
 *  Created on: Jan 6, 2021
 *      Author: julien.casanova
 */

#include <stdint.h>
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_adc.h"

uint32_t get_ADC_value(ADC_HandleTypeDef hadc1, uint32_t channel) {
	uint32_t adc_val = 0;

	ADC_ChannelConfTypeDef sConfig = {0};

	/** Configure Regular Channel
	*/
	sConfig.Channel = channel;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}

	HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1,100);
	adc_val = HAL_ADC_GetValue(&hadc1);

	return adc_val;
}

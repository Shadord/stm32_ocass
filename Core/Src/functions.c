
#include "function.h"

float convertFromADC_12B(u2_t value, float voltage) {
    return value*voltage/4.096;
}

float GET_Temp(ADC_HandleTypeDef hadc1, uint32_t channel) {
    return 190.0 - 50.0/272.0*convertFromADC_12B(get_ADC_value(hadc1, channel), 3.3);
}

float GET_Humid(TIM_HandleTypeDef htim1) {
    return 0;
}

void PowerTempSensor(bool on) {
	HAL_GPIO_WritePin(VDD_Temp_sensor_GPIO_Port, VDD_Temp_sensor_Pin, on ? GPIO_PIN_SET:GPIO_PIN_RESET);
	HAL_Delay(100);
}

void PowerFreqSensor(bool on) {
	HAL_GPIO_WritePin(VDD_Freq_Sensor_GPIO_Port, VDD_Freq_Sensor_Pin, on ? GPIO_PIN_SET:GPIO_PIN_RESET);
	HAL_Delay(100);
}

void freqCounter(bool state) {
	if(state == true) { // On active le timer + GPIO
		//HAL_TIM_Base_Start_IT(&htim6);
		HAL_NVIC_EnableIRQ(EXTI4_IRQn);
		frequenceCounter.counter = 0;
		frequenceCounter.enable = true;
	}else{ // Desactive tout*
		frequenceCounter.enable = false;
		//HAL_TIM_Base_Stop(&htim6);
		HAL_NVIC_DisableIRQ(EXTI4_IRQn);

	}
}


float cycle(float* temperature, float* humidite) {
	// Enable temperature sensor

	PowerTempSensor(true);
	  // Get temperature value
	  *temperature = GET_Temp(hadc1, ADC_Temp_Sensor_Channel);

	  // Disable temperature sensor
	  PowerTempSensor(false);

	  //freqCounter(true);

	  //while(frequenceCounter.enable);

	  //uint32_t counter = frequenceCounter.counter;

	  //*humidite = counter/2.0;


}
